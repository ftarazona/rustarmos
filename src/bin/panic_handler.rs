//panic_handler
//
//This binary is inspired by the documentation of cortex_m_rt 
// (https://docs.rs/cortex-m-rt/0.7.2/cortex_m_rt/)
//and by the practical work written by S. Tardieu for the SE202 course at Télécom Paris.
//
//I do not claim any intellectual property on this code.

//Do not use the standard library : in particular as there is no memory allocator every collection
//would for instance fail to execute.
#![no_std]

//Standard 'main' can not be used: here 'main'  will be specified by cortex_m_rt as the entry point
#![no_main]

use cortex_m_rt::entry;
use stm32l4 as _;   //PAC is not yet necessary. However, as it is declared it forces the device
                    //feature of cortex-m-rt to be true. cortex-m-rt hence relies on stm32l4 to
                    //populate the vtor

use rtt_target::{rtt_init_print, rprintln};
use panic_rtt_target as _;

//'main' is set to be the entry point of the program.
//As such it is not allowed to return.
#[entry]
fn main() -> !  {
    rtt_init_print!();
    rprintln!("Hello, world !");
    panic!("Program stopped");
}
