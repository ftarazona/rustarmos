# RustARMOS

This is a personal project for implementing basic RTOS services.

## Acknowledgements

Foremost I would like to thank my teachers at Télécom Paris, namely S. Tardieu, G. Duc, T. Graba, who allowed me to discover bare-metal programming on STM32L475 boards.

This project takes the same target, but goes beyond the scope of the pedagogical projects you will find in the syllabus of SE203 and SE202 courses.

SE203 was about C programming and real bare-metal, whereas SE202 was to teach Rust programming and made use of a HAL crate and an already implemented RTOS, namely rtic.
This project differs in that it is Rust programming at bare metal level, and it will eventually lead to implement by myself several OS services.


I would also like to acknowledge Rezel, an IT student association at Télécom Paris, who kindly let me use one of their boards for starting the project.
